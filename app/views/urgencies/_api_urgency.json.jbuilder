json.extract! api_urgency, :id, :created_at, :updated_at
json.url api_urgency_url(api_urgency, format: :json)
