json.type @deal.type_deal
json.value @deal.value
json.description @deal.description
json.trade_for @deal.trade_for
json.location @deal.location
json.user @deal.user

if @deal.urgency.present?
	json.urgency do |urgency|
		json.type @deal.urgency.type_urgency
		json.limit_date @deal.urgency.limit_date.strftime('%d/%m/%Y')
	end
end
json.photos do
	json.array! @deal.photos do |photo|
		json.src photo.src
	end
end