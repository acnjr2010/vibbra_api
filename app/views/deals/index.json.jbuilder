json.array! @deals do |deal|
	json.id deal.id
	json.type deal.type_deal
	json.description deal.description
	json.trade_for deal.trade_for
	json.location deal.location
	json.with_location current_user.location.present? ? true : false
	json.created_at deal.created_at.strftime('%d/%m/%Y')
	json.cover deal.photos.present? ? deal.photos.first.src : nil
end

