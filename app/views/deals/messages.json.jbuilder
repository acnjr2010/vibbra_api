json.array! @messages do |message|
	json.id message.id
	json.user message.user
	json.message message.message
	json.title message.title
	json.created_at message.created_at.strftime('%d/%m/%Y')
	json.answer Answer.where(message: message).present? ? Answer.where(message: message).first.answer : nil
end