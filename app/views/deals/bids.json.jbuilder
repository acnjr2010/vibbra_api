json.array! @bids do |bid|
	json.id bid.id
	json.user bid.user
	json.accepted bid.accepted
	json.value number_to_currency(bid.value, unit: 'R$ ', separator: ',', delimiter: '.')
	json.description bid.description
end