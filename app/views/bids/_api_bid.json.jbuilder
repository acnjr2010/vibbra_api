json.extract! api_bid, :id, :created_at, :updated_at
json.url api_bid_url(api_bid, format: :json)
