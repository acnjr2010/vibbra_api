json.extract! api_location, :id, :created_at, :updated_at
json.url api_location_url(api_location, format: :json)
