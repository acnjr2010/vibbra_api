json.name @user.name
json.email @user.email
json.login @user.login
json.password @user.encrypted_password

if @user.location.present?
	json.location do |location|
		json.lat @user.location.lat
		json.lng @user.location.lng
		json.address @user.location.address
		json.city @user.location.city
		json.state @user.location.state
		json.zip_code @user.location.zip_code
	end
end
