json.array! @bids do |bid|
	json.id bid.id
	json.description bid.description
	json.accepted bid.accepted
	json.value bid.value
	json.deal bid.deal
end