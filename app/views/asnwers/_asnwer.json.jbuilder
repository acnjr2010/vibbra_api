json.extract! asnwer, :id, :user_id, :message_id, :answer, :created_at, :updated_at
json.url asnwer_url(asnwer, format: :json)
