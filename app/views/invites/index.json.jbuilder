json.array! @invites do |invite|
	json.id invite.id
	json.name invite.name
	json.email invite.email
	json.created_at invite.created_at.strftime('%d/%m/%Y')
	json.status invite.status
end
