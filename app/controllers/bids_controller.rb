class BidsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_bid, only: [:show, :destroy]

  # GET /api/bids
  # GET /api/bids.json
  def index
    @bids = Bid.all
  end

  # GET /api/bids/1
  # GET /api/bids/1.json
  def show
  end

  # POST /api/bids
  # POST /api/bids.json
  def create
    @bid = Bid.new(description: params[:description], value: params[:value], user: current_user, deal: Deal.find(params[:id]))

    if @bid.save
      render json: @bid, status: :created
    else
      render json: @bid.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/bids/1
  # PATCH/PUT /api/bids/1.json
  def update
    @bid = Bid.find(params[:bid_id])

    if(!params[:accepted])
      @bid.destroy
    else
      if @bid.update(bid_params)
        render json: @bid, status: :ok
      else
        render json: @bid.errors, status: :unprocessable_entity
      end
    end
  end

  # DELETE /api/bids/1
  # DELETE /api/bids/1.json
  def destroy
    @bid.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bid
      @bid = Bid.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def bid_params
      params.require(:bid).permit(:user_id, :accepted, :value, :description, :created_at, :updated_at)
    end
end