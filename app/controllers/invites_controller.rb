class InvitesController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_invite, only: [:update, :destroy]

  # GET /api/invites
  # GET /api/invites.json
  def index
    @invites = Invite.where(user: current_user).order(name: :asc)
  end

  # GET /api/invites/1
  # GET /api/invites/1.json
  def show
    @invite = params[:token].present? ? Invite.where(invite_token: params[:token]).first : Invite.find(params[:invite_id])
  end

  # POST /api/invites
  # POST /api/invites.json
  def create
    user = Invite.create_new_user invite_params
    token = Invite.generate_token
    @invite = Invite.new(invite_params)
    @invite.user = current_user
    @invite.user_invited = user.id
    @invite.invite_token = token

    if @invite.save
      ApplicationMailer.send_invite(@invite).deliver_now
      render :show, status: :created
    else
      render json: @invite.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/invites/1
  # PATCH/PUT /api/invites/1.json
  def update
    @invite = Invite.find(params[:invite_id])
    @user = User.find(@invite.user_invited)
    @user.update_attributes(name: invite_params[:name], email: invite_params[:email])

    if @invite.update(invite_params)
      ApplicationMailer.send_invite(@invite).deliver_now if invite_params[:status] == 'Pendente'
      render :show, status: :ok
    else
      render json: @invite.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invite
      @invite = Invite.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def invite_params
      params.require(:invite).permit(:name, :email, :user_id,
        :user_invited, :status, :invite_token, :created_at, :updated_at)
    end
end
