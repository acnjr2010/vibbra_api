class LocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_location, only: [:show, :update, :destroy]

  # GET /api/locations
  # GET /api/locations.json
  def index
    @locations = Location.all
  end

  # GET /api/locations/1
  # GET /api/locations/1.json
  def show
  end

  # POST /api/locations
  # POST /api/locations.json
  def create
    @location = Location.new(location_params)

    if @location.save
      render :show, status: :created, location: @location
    else
      render json: @location.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/locations/1
  # PATCH/PUT /api/locations/1.json
  def update
    if @location.update(location_params)
      render :show, status: :ok, location: @location
    else
      render json: @location.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/locations/1
  # DELETE /api/locations/1.json
  def destroy
    @location.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def location_params
      params.require(:location).permit(:lat, :lng, :address,
        :city, :state, :zip_code, :created_at, :updated_at)
    end
end
