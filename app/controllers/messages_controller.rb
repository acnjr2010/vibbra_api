class MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_message, only: [:show, :update, :destroy]

  # GET /api/messages
  # GET /api/messages.json
  def index
    @messages = Message.all
  end

  # GET /api/messages/1
  # GET /api/messages/1.json
  def show
  end

  # POST /api/messages
  # POST /api/messages.json
  def create
    @message = Message.new(message_params)
    @message.user = current_user
    @message.deal = Deal.find(params[:id])

    if @message.save
      render :show, status: :created, location: @message
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/messages/1
  # PATCH/PUT /api/messages/1.json
  def update
    if @message.update(message_params)
      render :show, status: :ok, location: @message
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/messages/1
  # DELETE /api/messages/1.json
  def destroy
    @message.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def message_params
      params.require(:message).permit(:user_id, :deal_id,
        :title, :message, :created_at, :updated_at)
    end
end
