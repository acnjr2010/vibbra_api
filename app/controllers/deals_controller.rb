class DealsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_deal, only: [:show, :update, :destroy, :bids, :messages]

  # GET /deals
  # GET /deals.json
  def index
    @current_user_location = current_user.location.present? ? current_user.location.city : nil
    @deals =  @current_user_location.present? ? Deal.joins(:location).where('locations.city = ?', @current_user_location).count > 0 ? Deal.joins(:location).where('locations.city = ?', @current_user_location).order(created_at: :desc) : Deal.all.order(created_at: :desc) : Deal.all.order(created_at: :desc)
  end

  # GET /deals/1
  # GET /deals/1.json
  def show
  end

  # POST /deals
  # POST /deals.json
  def create
    @location = Location.new(location_params)
    @urgency = Urgency.new(urgency_params)
    @deal = Deal.new(deal_params)

    if @location.save && @urgency.save
      @deal.location = @location
      @deal.urgency = @urgency
      @deal.user = current_user
      if @deal.save
        params[:photos].each do |photo|
          Photo.create(
            src: photo[:src],
            deal: @deal
          )
        end

        render json: @deal, status: :created
      else
        render json: @deal.errors, status: :unprocessable_entity
      end
    else
      render json: [@location.errors, @urgency.errors], status: :unprocessable_entity
    end
  end

  # PATCH/PUT /deals/1
  # PATCH/PUT /deals/1.json
  def update
    @deal.type_deal = params[:deal][:type]
    if @deal.update(deal_params)
      @deal.location.update(location_params)
      @deal.urgency.update(urgency_params)
      @photos = Photo.where(deal:@deal).pluck(:src)
      @new_photos = params[:photos].pluck(:src)

      @photos.each do |photo|
        unless @new_photos.include?(photo)
          delete_photo = Photo.where(deal: @deal, src: photo).first
          delete_photo.destroy
        end
      end

      @photos = Photo.where(deal:@deal).pluck(:src)

      @new_photos.each do |photo|
        unless @photos.include?(photo)
          Photo.create(
            deal: @deal,
            src: photo
          )
        end
      end

      render json: @deal, status: :ok
    else
      render json: @deal.errors, status: :unprocessable_entity
    end
  end

  # DELETE /deals/1
  # DELETE /deals/1.json
  def destroy
    @deal.destroy
  end

  def bids
    @bids = @deal.bids
  end

  def messages
    @messages = @deal.messages
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deal
      @deal = Deal.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def deal_params
      params.require(:deal).permit(
        :type_deal,
        :value,
        :description,
        :trade_for,
        :created_at,
        :updated_at
      )
    end

    def location_params
      params.require(:location).permit(
        :lat,
        :lng,
        :address,
        :city,
        :state,
        :zip_code
      )
    end

    def urgency_params
      params.require(:urgency).permit(
        :type_urgency,
        :limit_date
      )
    end
end
