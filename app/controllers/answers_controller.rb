class AnswersController < ApplicationController
  before_action :set_answer, only: [:show, :update, :destroy]

  # GET /answers
  # GET /answers.json
  def index
    @answers = Answer.all
  end

  # GET /answers/1
  # GET /answers/1.json
  def show
  end

  # POST /answers
  # POST /answers.json
  def create
    @answer = Answer.new(answer_params)
    @answer.user = current_user
    @answer.message_id = params[:message_id]

    if @answer.save
      render json: @answer, status: :created
    else
      render json: @answer.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /asnwers/1
  # PATCH/PUT /asnwers/1.json
  def update
    if @answer.update(answer_params)
      render :show, status: :ok, location: @answer
    else
      render json: @answer.errors, status: :unprocessable_entity
    end
  end

  # DELETE /asnwers/1
  # DELETE /asnwers/1.json
  def destroy
    @answer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ansswer
      @answer = Answer.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def answer_params
      params.require(:answer).permit(:user_id, :message_id, :answer)
    end
end
