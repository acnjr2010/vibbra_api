class UsersController < ApplicationController
  before_action :authenticate_user!, except: [:active_invite]
  before_action :set_user, only: [:show, :update, :destroy, :deals, :bids]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    render json: @users
  end


  # GET /users/1
  # GET /users/1.json
  def show
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      render :show, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if current_user.update!(user_params)
      current_user.location.update_attributes(
        lat: params[:location][:lat].to_f,
        lng: params[:location][:lng].to_f,
        address: params[:location][:address],
        city: params[:location][:city],
        state: params[:location][:state],
        zip_code: params[:location][:zip_code].to_i
      )
      render :show, status: :ok, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
  end

  def active_invite
    invite = Invite.where(invite_token: params[:invite_token][:token]).first
    if invite.present?
      @user = User.find(invite.user_invited)
      location = Location.create(location_params)
      @user.location = location

      if @user.update(user_params)
        invite.update_attributes(status: 1)
        render :show, status: :ok, location: @user
      else
        render json: user.errors, status: :unprocessable_entity
      end
    else
      render json: invite.errors, status: :unprocessable_entity
    end
  end

  # get all current_user deals
  def deals
    @deals = @user.deals
  end

  def bids
    @bids = @user.bids
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = current_user
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(
        :email,
        :encrypted_password,
        :reset_password_token,
        :reset_password_sent_at,
        :remember_created_at,
        :name,
        :login,
        :created_at,
        :updated_at,
        :location_id,
        :password
      )
    end

    def location_params
      params.require(:location).permit(
        :lat,
        :lng,
        :address,
        :city,
        :state,
        :zip_code
      )
    end
end
