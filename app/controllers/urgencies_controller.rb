class UrgenciesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_urgency, only: [:show, :update, :destroy]

  # GET /api/urgencies
  # GET /api/urgencies.json
  def index
    @urgencies = Urgency.all
  end

  # GET /api/urgencies/1
  # GET /api/urgencies/1.json
  def show
  end

  # POST /api/urgencies
  # POST /api/urgencies.json
  def create
    @urgency = Urgency.new(urgency_params)

    if @urgency.save
      render :show, status: :created, location: @urgency
    else
      render json: @urgency.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/urgencies/1
  # PATCH/PUT /api/urgencies/1.json
  def update
    if @urgency.update(urgency_params)
      render :show, status: :ok, location: @urgency
    else
      render json: @urgency.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/urgencies/1
  # DELETE /api/urgencies/1.json
  def destroy
    @urgency.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_urgency
      @urgency = Urgency.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def urgency_params
      params.require(:urgency).permit(:type_urgency, :limit_date,
        :created_at, :updated_at)
    end
end
