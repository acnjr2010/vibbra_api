class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :jwt_authenticatable,
          jwt_revocation_strategy: JwtBlacklist

  validates :name, :email, presence: true
  validates_uniqueness_of :email, :login

  # associations
  belongs_to :location, optional: true
  has_many :deals
  has_many :bids
end
