class Urgency < ApplicationRecord
	enum type_urgency: ['1 – Baixa', '2 – Média', '3 – Alta', '4 – Data']

	validates :type_urgency, presence: true
end
