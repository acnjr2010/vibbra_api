class Message < ApplicationRecord
  belongs_to :answer, optional: true
  belongs_to :user
  belongs_to :deal
  has_many :bids
end
