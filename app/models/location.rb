class Location < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :deal, optional: true

  validates :zip_code, :lat, :lng, :address, :city, :state, presence: true
end
