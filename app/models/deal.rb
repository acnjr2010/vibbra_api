class Deal < ApplicationRecord
  # associations
  belongs_to :location
  belongs_to :user
  belongs_to :urgency, optional: true
  has_many :photos
  has_many :messages
  has_many :bids

 	#enum
  enum type_deal: ['1 – Venda', '2 – Troca', '3 – Desejo']

  validates :type_deal, :location, :urgency, :user, :description, presence: true
end
