class Invite < ApplicationRecord
  belongs_to :user

  validates :name, :user_invited, presence: true

  enum status: ['Pendente', 'Aceito', 'Cancelado']

  def self.create_new_user params
  	user = User.new(
  		name: params[:name],
  		email: params[:email],
  		password: '12345678',
  		login: "##{params[:name]}_#{Date.today}@"
  	)

  	user.save
  	user
  end

  def self.generate_token
  	SecureRandom.hex(30)
  end
end