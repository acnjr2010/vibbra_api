class Photo < ApplicationRecord
  belongs_to :deal

  validates :src, presence: true
end
