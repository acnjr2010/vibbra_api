class ApplicationMailer < ActionMailer::Base
  default from: 'ecommercetestevibbra@gmail.com'
  layout 'mailer'

  def send_invite invite
  	@name = invite.name
  	@token = invite.invite_token
  	@user_name = invite.user.name

  	mail to: invite.email, subject: 'Convite para o e-commerce.'
  end
end
