# Vibbra teste backend

## Este aplicativo substitui a api dado pelo Sr. Vibbraneo no desenvolvimento do teste

## Setup

```
git clone
cd vibbra_api
bundle install
bundle exec rails db:create
bundle exec rails db:migrate
bundle exec rails db:seed
```

- Vai gerar o usuário `admin@vibbra.com` senha: `12345678`
- Para subir o sistema: `rails server -p 3001`


## Observação importante para rodar localmente

- Não utilize o rails s somente pois ele irá rodar na porta 3000 que por default será usada para subir o frontend que consome esse back
- O front end tem que atualizar o arquivo `api.js` para que ele não busque as informações da produção e sim do seu banco local.

## Versão online

[E-commerce vibbra](https://vibbra-front.herokuapp.com/login)