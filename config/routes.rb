Rails.application.routes.draw do
  resources :asnwers
  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'authenticate',
               sign_out: 'logout'
             },
             controllers: {
               sessions: 'sessions',
             }

  resources :deals, only: [:index], format: "json"
  put '/user/:id', to: 'users#update', format: 'json'
  post '/user/:id', to: 'users#create', format: 'json'
  put '/user/:id/active_invite', to: 'users#active_invite', format: 'json'
  get '/user/:id/deals', to: 'users#deals', format: 'json'
  get '/user/:id/bids', to: 'users#bids', format: 'json'
  get '/user/:id/invite', to: 'invites#index', format: "json"
  post '/user/:id/invite', to: 'invites#create', format: "json"
  get '/active_invite/:token', to: 'invites#show', format: "json"
  delete '/invite/:id', to: 'invites#destroy', format: 'json'
  put '/user/:id/invite/:invite_id', to: 'invites#update', format: 'json'
  get '/user/:id/invite/:invite_id', to: 'invites#show', format: 'json'
  get '/deal/:id', to: 'deals#show', format: 'json'
  get '/deal/:id/bid', to: 'deals#bids', format: 'json'
  put '/deal/:id/bid/:bid_id', to: 'bids#update', format: 'json'
  get '/deal/:id/message', to: 'deals#messages', format: 'json'
  post '/deal/:id/message/:message_id/answer', to: 'answers#create', format: 'json'
  post '/deal/:id/bid', to: 'bids#create', format: 'json'
  post '/deal/:id/message', to: 'messages#create', format: 'json'
  put '/deal/:id', to: 'deals#update', format: 'json'
  post '/deal', to: 'deals#create'
end
