class AddStatusToInvites < ActiveRecord::Migration[5.2]
  def self.up
    add_column :invites, :status, :integer, default: 0
    add_column :invites, :invite_token, :string
  end

  def self.down
  	remove_column :invites, :status
  	remove_column :invites, :invite_token
  end
end
