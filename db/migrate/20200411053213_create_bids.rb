class CreateBids < ActiveRecord::Migration[5.2]
  def self.up
    create_table :bids do |t|
      t.references :user, foreign_key: true
      t.boolean :accepted, default: false
      t.decimal :value
      t.string :description

      t.timestamps
    end
  end

  def self.down
  	drop_table :bids
  end
end
