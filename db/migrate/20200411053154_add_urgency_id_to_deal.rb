class AddUrgencyIdToDeal < ActiveRecord::Migration[5.2]
  def self.up
    add_reference :deals, :urgency, foreign_key: true
  end

  def self.down
  	remove_reference :deals, :urgency
  end
end
