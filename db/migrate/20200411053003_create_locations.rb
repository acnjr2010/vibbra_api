class CreateLocations < ActiveRecord::Migration[5.2]
  def self.up
    create_table :locations do |t|
      t.float :lat
      t.float :lng
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip_code

      t.timestamps
    end
  end

  def self.down
    drop_table :locations
  end
end
