class CreateInvites < ActiveRecord::Migration[5.2]
  def self.up
    create_table :invites do |t|
      t.string :name
      t.string :email
      t.references :user, foreign_key: true
      t.integer :user_invited

      t.timestamps
    end
  end

  def self.down
  	drop_table :invites
  end
end
