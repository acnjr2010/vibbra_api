class RenameColumnTypeInDeal < ActiveRecord::Migration[5.2]
  def self.up
  	rename_column :deals, :type, :type_deal
  end

  def self.down
  	rename_column :deals, :type_deal, :type
  end
end
