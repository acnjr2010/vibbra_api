class CreateMessages < ActiveRecord::Migration[5.2]
  def self.up
    create_table :messages do |t|
      t.references :user, foreign_key: true
      t.references :deal, foreign_key: true
      t.string :title
      t.string :message

      t.timestamps
    end
  end

  def self.down
  	drop_table :messages
  end
end
