class AddLocationIdToUsers < ActiveRecord::Migration[5.2]
  def self.up
    add_reference :users, :location, foreign_key: true
  end

  def self.down
  	remove_reference :users, :location
  end
end
