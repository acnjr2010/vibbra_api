class CreateUrgencies < ActiveRecord::Migration[5.2]
  def self.up
    create_table :urgencies do |t|
      t.integer :type_urgency, default: 0
      t.date :limit_date

      t.timestamps
    end
  end

  def self.down
  	drop_table :urgencies
  end
end
