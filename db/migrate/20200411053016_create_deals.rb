class CreateDeals < ActiveRecord::Migration[5.2]
  def self.up
    create_table :deals do |t|
      t.integer :type, default: 0
      t.decimal :value
      t.string :description
      t.string :trade_for
      t.references :location, foreign_key: true

      t.timestamps
    end
  end

  def self.down
    drop_table :deals
  end
end
