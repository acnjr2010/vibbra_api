class CreateAnswers < ActiveRecord::Migration[5.2]
  def self.up
    create_table :answers do |t|
      t.references :user, foreign_key: true
      t.references :message, foreign_key: true
      t.string :answer

      t.timestamps
    end
  end

  def self.down
  	drop_table :answers
  end
end
