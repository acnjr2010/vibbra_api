class CreatePhotos < ActiveRecord::Migration[5.2]
  def self.up
    create_table :photos do |t|
      t.string :src
      t.references :deal, foreign_key: true

      t.timestamps
    end
  end

  def self.down
  	drop_table :photos
  end
end
