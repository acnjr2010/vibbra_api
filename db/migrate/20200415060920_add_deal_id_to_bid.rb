class AddDealIdToBid < ActiveRecord::Migration[5.2]
  def self.up
    add_reference :bids, :deal, foreign_key: true
  end

  def self.down
  	remove_reference :bids, :deal
  end
end
