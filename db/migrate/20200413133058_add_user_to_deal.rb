class AddUserToDeal < ActiveRecord::Migration[5.2]
  def self.up
    add_reference :deals, :user, foreign_key: true
  end

  def self.down
  	remove_reference :deals, :user
  end
end
