# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Location.create(lat: -18.9658, lng: -43.5412, address: 'Avenida Sambaiatuba, 110', city: 'São Vicente', state: 'SP', zip_code: 11365140)
Location.create(lat: -18.2587, lng: -43.4152, address: 'Frei Gaspar, 202', city: 'São Vicente', state: 'SP', zip_code: 11300140)
Location.create(lat: -18.5547, lng: -43.7584, address: 'Av Paulista, 2100', city: 'São Paulo', state: 'SP', zip_code: 11365140)

User.create(name: 'Admin', password: 'admin', login: 'admin', email: 'admin@vibbra.com.br')
User.create(name: 'Antonio Carlos', password: '12345678', login: '12345678', email: 'dev.antoniocarlos@gmail.com', location_id: 1)
User.create(name: 'Bianca Liz da Cunha', password: '12345678', login: 'bianca', email: 'bianca@trash-mail.com')
User.create(name: 'Yago Augusto Rodrigues', password: '12345678', login: 'yago', email: 'yago@trash-mail.com', location_id: 2)
User.create(name: 'Manoel Caio Ramos', password: '12345678', login: 'manoel', email: 'manoel@trash-mail.com', location_id: 3)




