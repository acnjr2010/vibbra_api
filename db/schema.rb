# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_15_222430) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "answers", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "message_id"
    t.string "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_id"], name: "index_answers_on_message_id"
    t.index ["user_id"], name: "index_answers_on_user_id"
  end

  create_table "bids", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "accepted", default: false
    t.decimal "value"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "deal_id"
    t.index ["deal_id"], name: "index_bids_on_deal_id"
    t.index ["user_id"], name: "index_bids_on_user_id"
  end

  create_table "deals", force: :cascade do |t|
    t.integer "type_deal", default: 0
    t.decimal "value"
    t.string "description"
    t.string "trade_for"
    t.bigint "location_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "urgency_id"
    t.bigint "user_id"
    t.index ["location_id"], name: "index_deals_on_location_id"
    t.index ["urgency_id"], name: "index_deals_on_urgency_id"
    t.index ["user_id"], name: "index_deals_on_user_id"
  end

  create_table "invites", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.bigint "user_id"
    t.integer "user_invited"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0
    t.string "invite_token"
    t.index ["user_id"], name: "index_invites_on_user_id"
  end

  create_table "jwt_blacklists", force: :cascade do |t|
    t.string "jti"
    t.datetime "exp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["jti"], name: "index_jwt_blacklists_on_jti"
  end

  create_table "locations", force: :cascade do |t|
    t.float "lat"
    t.float "lng"
    t.string "address"
    t.string "city"
    t.string "state"
    t.integer "zip_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "deal_id"
    t.string "title"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deal_id"], name: "index_messages_on_deal_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "photos", force: :cascade do |t|
    t.string "src"
    t.bigint "deal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deal_id"], name: "index_photos_on_deal_id"
  end

  create_table "urgencies", force: :cascade do |t|
    t.integer "type_urgency", default: 0
    t.date "limit_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "name"
    t.string "login"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "location_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["location_id"], name: "index_users_on_location_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "answers", "messages"
  add_foreign_key "answers", "users"
  add_foreign_key "bids", "deals"
  add_foreign_key "bids", "users"
  add_foreign_key "deals", "locations"
  add_foreign_key "deals", "urgencies"
  add_foreign_key "deals", "users"
  add_foreign_key "invites", "users"
  add_foreign_key "messages", "deals"
  add_foreign_key "messages", "users"
  add_foreign_key "photos", "deals"
  add_foreign_key "users", "locations"
end
